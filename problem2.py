#Inicialization of libraries
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.sparse import diags
import scipy.linalg as linalg
import scipy.sparse.linalg as splinalg
from matplotlib import animation
from matplotlib.animation import FFMpegWriter
from IPython.display import HTML
from matplotlib.animation import FuncAnimation
matplotlib.use("Agg")


### Parameters
L=10. # length of the interval
N=200 # number of nodes
tm=17#20
K=200
omega=0.4 # driving frequency
nbstates=20 # size of the basis (should be < N)
assert(nbstates<N)
theta=0.5
### Discretization
dx=L/N
x=dx/2 + dx*np.arange(N)
dt=tm/K
t=dt/2 + dt*np.arange(K)


#creation of pre-animation
fig = plt.figure()
ax = plt.axes(xlim=(0, 9), ylim=(-1, 100))
line, = ax.plot([], [], lw=4)


#frame updating
def init():
    line.set_data([], [])
    return line,


### Potential of coordinate
def V(x):
    return (x-0.5*L)**2


#Potentional of coordinate and time
def U(x,t):
    return x*np.sin(omega*t)


#initial wavefunction
def psi0(x):
    return np.exp(-(x-0.3*L)**2)


### Construct matrix
g=1./(dx*dx)
diagonal = g+V(x)
subdiagonal = -0.5*g*np.ones(N-1) # same as: np.array([-0.5*g]*(N-1))
Msparse = diags([diagonal,subdiagonal,subdiagonal], [0, 1, -1])


#eigenvectors and values
#Msparse.toarray() # convert to dense matrix to display the result
### Calculate eigenvalues, eigenvectors
#evals, evecs = splinalg.eigsh(Msparse, sigma=0, k=Nstates)
evals, evecs = splinalg.eigsh(Msparse.tocsc(), sigma=0, k=nbstates)
print(np.diag(evals))
### An alternative version
tevals, tevecs = linalg.eigh_tridiagonal(diagonal,subdiagonal)


### Normalization of wavefunctions
phi = evecs/np.sqrt(dx)
for nv in range(5):
    overlap = dx*np.vdot(phi[:,nv], phi[:,nv]) # same as dx*np.sum( np.conj)

C = np.zeros(nbstates,dtype=complex)
#psi=np.matmul(phi,C)

#creation of full hamiltonian H(x,t)
def H(x,t):
    Umatrix = np.zeros((nbstates,nbstates),dtype=complex)
    for n in range(nbstates):
        for m in range(nbstates):
            #Umatrix[n,m] = dx*np.vdot(phi[:,nv], U(x,t)*phi[:,nv])#matrix element
            Umatrix[n,m] = np.sum(U(x,t)*np.conj(phi[:,n])*phi[:,m]*dx)
    #H = Umatrix +np.diag(evals)
    return(Umatrix +np.diag(evals))#матричное сложение

#K=int(tm/dt)
#K=200

#print(Ntimes)

##################A[tn+1]C[tn+1]=B[tn]C[tn]


#creation of matrix A 
def A(t):
    #A=1j/dt-theta*H(x,t)
    A=1j/dt*np.eye(nbstates,dtype=complex)-theta*H(x,t)#на диагонали находятся 1j/dt
    return(A)


#creation of matrix B 
def B(t):
    #B=(1-theta)*H(x,t)+1j/dt
    B=(1-theta)*H(x,t)+1j/dt*np.eye(nbstates,dtype=complex)#на диагонали находятся 1j/dt
    return(B)

#Формирование матрицы коэффициентов Cn
#print(B)
Cn=[C*K]
#Cn=[]
#Cn=np.zeros(
#print(Cn)
for i in range(K):###без этого цикла программа не работает
    Cn.append(C)
Cn=np.transpose(np.array(Cn))
#print(Cn)
# Initial values for the wavefunction
#np.shape(Cn)


#Заполнение коэффициентов Cn 3начениями
for i in range(nbstates):
    Cn[i,0]=np.sum(np.conj(phi[:,i])*psi0(x)*dx)#заполнение первого столбика начальными значениями
#print(Cn)    
for i in range(1,K): 
    Cn[:,i]=np.linalg.solve(A(dt*(i+1)),np.dot(B((dt*i)),Cn[:,i-1]))#заполнение оставшихся столбиков значениями
#print(Cn)


#creation of re-animation
def animate(i):
    psiwave=np.zeros(phi[:,0].shape)
    for j in range(phi.shape[1]):
        psiwave=psiwave+phi[:,j]*Cn[j,i]#*np.exp(-1j*evals[j]*i)
        #psiwave=phi[:,j]*Cn[j,i]
    #psiwave=phi@Cn
        #psiwave =psiwave+np.matmul(phi,Cn)  
    line.set_data(x, psiwave*np.conj(psiwave))
    return line,

 
#final animation and saving video
anima = FuncAnimation(fig, animate, init_func=init,
                                  frames=80, interval=120, blit=True)
FFwriter=FFMpegWriter(fps=5, metadata=dict(artist='Me'), bitrate=1800)
HTML(anima.to_html5_video())
anima.save('psiwave5.mp4', writer=FFwriter)
